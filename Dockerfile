FROM ubuntu:latest

RUN \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y wget tar netcat && \
  apt-get install -y bluez
RUN mkdir /ruuvitag && cd /ruuvitag && \
wget https://github.com/lautis/ruuvitag-listener/releases/download/v0.5.3/ruuvitag-listener-v0.5.3-x86_64-unknown-linux-gnu.tar.gz && \
tar -xf *
