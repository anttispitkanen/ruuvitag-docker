#!/bin/sh

while [ ! -S /tmp/telegraf/telegraf-ruuvitag.sock ]
do
  >&2 echo "Socket file not found - sleeping"
  sleep 2 # or less like 0.2
done

/ruuvitag/ruuvitag-listener | netcat -q0 -U /tmp/telegraf/telegraf-ruuvitag.sock