# Ruuvitag 

Binds together:
- [Ruuvitag-listener](https://github.com/lautis/ruuvitag-listener), for listening the Ruuvitag beacon messages via BLE
- [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/), to read the measurement data and pass it to ..
- [InfluxDB](https://www.influxdata.com/products/influxdb/), time-series database
- [Grafana](https://grafana.com/), time-series visualization tool 

All running in Docker without stuff installed in the host

## Contents

- `docker-compose.yml`, Docker compose file for defining the system 
- `Dockerfile`, containerizing Ruuvitag-listener
- `influxdb.evn.template`, template for defining the DB and USER&PASS of Influxdb
- `telegraf.env.template`, template for defining Tags 
- `telegraf.conf`, telegraf configuration file from ruuvitag-listener repo adjusted for the containerization
- `grafana\*.json`, grafana dashboards 
## Usage
1. Name your tags
```bash
cp telegraf.env.template telegraf.env
```
and set the MAC addresses for your tags on the new `telegraf.env`-file. You can get the MAC addresses for your tag from iOS or Android Ruuvi software. The template has three Ruuvis, match your amount with the config and also modify `ruuvitag.conf` accordingly (section `[[processors.regex.tags]]`).

If you somehow make a mistake in this step your tags will show up at Influxdb&Grafana just with their MAC address as their name.

2. Set a user and password for influxdb 
```bash
cp influxdb.env.template influxdb.env
```
and just add username and password in the `influxdb.env`-file

3. Get the ship flying

```bash
docker-compose up 
```
Check what is happening in the communication

4. Point your web browser to Grafana @ http://localhost:3000 and add Influxdb as Grafana source with `http://influxdb:8086` as the URL and `telegraf` as the DB name

5. Import dashboards


![grafana_internet](img/grafana_internet.png)
![grafana_temperature](img/grafana_temperature.png)

### Running as a service
If you want to run the service in the background use 
```bash
docker-compose up -d
```
to run the Docker compose as a daemon. With `restart: always` set on Docker compose -file, the system should spring up a boot

## Debugging

### Using WSL in Windows

Sorry, WSL does not support exposing hardware and that means bluetooth only. Won't work, found out the hard way.

### Host machine
If everything does not go like in movies, it might be beneficial to start debugging from the host machine

In order for these steps to work, you have to have `bluez` installed with the package manager of your choice. 

1. Is there a BT device in the house
```bash
hcitool dev
```
should list  some BT Devices.

2. Is the ruuvitag listener getting anything
   
Get the binary from [Ruuvitag-listener](https://github.com/lautis/ruuvitag-listener) on your host and check whether you can get data from your dongles

### Containers

1. What is happening with my stack?
Check the `docker-compose up` output

2. Maybe the listener is broken? 
You can get inside the listener container via `docker compose exec ruuvitag-listener sh`. Run the `write_to_socket.sh` for surprising results.  
